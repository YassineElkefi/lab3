CC=gcc
CFLAGS=-std=c99 -Wall

# additional flags for gcov
TESTFLAGS=-fprofile-arcs -ftest-coverage

app : jeu.h jeu.c app.c
	# build the app
	$(CC) $(CFLAGS) -o app app.c jeu.c

	# run the app
	./app

clean:
	rm -f *.o app *.gcov *.gcda *.gcno
