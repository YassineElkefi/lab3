#include "jeu.h"
#include <stdio.h>

int main() {
    char choixJoueur;
    char choixOrdinateur;

    printf("Choisissez R (pierre), P (papier) ou C (ciseaux) : ");
    scanf(" %c", &choixJoueur);

    if (choixJoueur != 'R' && choixJoueur != 'P' && choixJoueur != 'C') {
        printf("Choix invalide. Veuillez choisir R, P ou C.\n");
        return 1;
    }

    choixOrdinateur = hasard();
    int resultat = comparaison(choixJoueur, choixOrdinateur);

    switch (resultat) {
        case 1:
            printf("Le joueur gagne !\n");
            break;
        case -1:
            printf("L'ordinateur gagne !\n");
            break;
        default:
            printf("Égalité !\n");
            break;
    }

    return 0;
}
