#include <stdlib.h>
#include <time.h>
#include "jeu.h"

int comparaison(char choixJoueur, char choixOrdinateur) {
	if (choixJoueur == choixOrdinateur) {
        return 0; // Égalité
    } else if ((choixJoueur == 'R' && choixOrdinateur == 'C') ||
               (choixJoueur == 'P' && choixOrdinateur == 'R') ||
               (choixJoueur == 'C' && choixOrdinateur == 'P')) {
        return 1; // Le joueur gagne
    } else {
        return -1; // L'ordinateur gagne
    }
}

char hasard() {
    srand(time(NULL));
    int nombreAleatoire = rand() % 3;
    if (nombreAleatoire == 0) {
        return 'R';
    } else if (nombreAleatoire == 1) {
        return 'P';
    } else {
        return 'C';
    }
}
